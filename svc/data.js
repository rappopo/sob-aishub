const { _, Sequelize, DateTime, age, createError, parseDuration } = require('rappopo-sob').Helper
const { FetchUrl } = require('rappopo-sob').Factory
const { Persistence } = require('rappopo-sob').Factory

const schema = {
  id: { type: Sequelize.STRING(100), allowNull: false, primaryKey: true },
  ts: Sequelize.DATE,
  imo: Sequelize.STRING(20),
  name: Sequelize.STRING(50),
  callSign: Sequelize.STRING(20),
  shipType: Sequelize.INTEGER,
  shipClass: Sequelize.STRING(20),
  flag: Sequelize.STRING(2),
  lat: Sequelize.DOUBLE,
  lng: Sequelize.DOUBLE,
  cog: Sequelize.DOUBLE,
  sog: Sequelize.DOUBLE,
  heading: Sequelize.DOUBLE,
  navStat: Sequelize.INTEGER,
  rot: Sequelize.INTEGER,
  toBow: Sequelize.INTEGER,
  toStern: Sequelize.INTEGER,
  toStarboard: Sequelize.INTEGER,
  toPortside: Sequelize.INTEGER,
  draught: Sequelize.DOUBLE,
  dest: Sequelize.STRING(50),
  eta: Sequelize.STRING(20),
  accuracy: Sequelize.BOOLEAN,
  epfd: Sequelize.INTEGER
}

const fields = _.keys(schema)
let countries = []
let classes = []

module.exports = ({ sob, sd, sobr }) => {
  return {
    mixins: [sd, Persistence(), FetchUrl({ sobr })],
    model: {
      define: schema,
      options: {
        timestamps: false,
        underscored: true
      }
    },
    dependencies: ['marineSdShipClass'],
    settings: {
      webApi: { readonly: true },
      singleService: true,
      fields,
      cron: [{
        time: sob.config.cronTime,
        timeout: '5m',
        autoStart: true,
        runOnAutoStart: true,
        handler: 'fetchApi'
      }, {
        time: '* * * * *',
        timeout: '5m',
        autoStart: true,
        runOnAutoStart: true,
        handler: 'purgeInactive'
      }]
    },
    afterLoad () {
      this.broker.call('coreMiscCountry.find', { fields: ['id', 'mmsi'] })
        .then(result => {
          countries = _.map(result, r => {
            r.mmsi = _.map((r.mmsi || '').split(','), m => _.trim(m))
            return r
          })
          return this.broker.call('marineSdShipClass.find')
        })
        .then(result => {
          classes = _.map(result, c => {
            c.aisShipType = (c.aisShipType || '').split(',')
            return c
          })
        })
        .catch(err => {
          if (err) {}
        })
    },
    actions: {
      async fetchApi (ctx) {
        const start = DateTime.local()
        if (_.isEmpty(sob.config.username)) throw createError('Username is empty')
        const url = `http://data.aishub.net/ws.php?username=${sob.config.username}&format=1&output=json&compress=0`
        let result = await this.fetchUrl(url)
        if (_.isEmpty(result)) throw createError('No result')
        if (result[0].ERROR) throw createError(result[0].ERROR_MESSAGE)
        result = result[1] || []
        let count = 0
        for (let i = 0; i < result.length; i++) {
          try {
            const country = _.find(countries, c => c.mmsi.includes((result[i].MMSI + '').substr(0, 3))) || {}
            const flag = country.id
            const cls = _.find(classes, c => c.aisShipType.includes(result[i].TYPE + '')) || {}
            const shipClass = cls.id
            const rec = {
              id: result[i].MMSI + '',
              ts: DateTime.fromSQL(result[i].TIME).toISO(),
              imo: result[i].IMO,
              name: result[i].NAME,
              callSign: result[i].CALLSIGN,
              shipType: result[i].TYPE,
              shipClass,
              flag,
              lat: result[i].LATITUDE,
              lng: result[i].LONGITUDE,
              cog: result[i].COG === 360 ? -9999 : result[i].COG,
              sog: result[i].SOG === 102.4 ? -9999 : result[i].SOG,
              heading: result[i].HEADING === 511 ? -9999 : result[i].HEADING,
              navStat: result[i].NAVSTAT,
              rot: result[i].ROT,
              toBow: result[i].A,
              toStern: result[i].B,
              toStarboard: result[i].D,
              toPortside: result[i].C,
              draught: result[i].DRAUGHT,
              dest: result[i].DEST,
              eta: result[i].ETA,
              accuracy: result[i].PAC === 1,
              epfd: result[i].DEVICE
            }
            if (rec.sog !== -9999) rec.sog = rec.sog * 1.852
            await this.broker.call('aishubData.modelUpsert', rec)
            count++
          } catch (err) {}
        }
        return `processed ${count}/${result.length} records in ${age(start)}`
      },
      async purgeInactive (ctx) {
        const start = DateTime.local()
        const milliseconds = parseDuration(sob.config.inactivityThreshold)
        const dt = DateTime.utc().minus({ milliseconds }).toJSDate()
        const result = await this.broker.call('aishubData.find', { query: { ts: { $lt: dt } } })
        const ids = _.map(result, 'id')
        if (ids.length === 0) return false
        await this.adapter.model.destroy({ where: { id: { $in: ids } } })
        return `purge ${ids.length} old records in ${age(start)}`
      }
    }
  }
}
